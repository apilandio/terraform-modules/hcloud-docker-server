terraform {
  required_providers {
    hcloud = {
      source = "hetznercloud/hcloud"
      version = "~> 1.21"
    }
  }
}

provider "hcloud" {
  token = var.hcloud_access_token
}

locals {
  common_tags = {
    terraform = true
  }
  volume_name = format("%s-%s",var.hostname, "docker-data")
  ssh_key_name = format("%s-%s",var.hostname, "terraform-ssh-key")
}

resource "hcloud_ssh_key" "terraform" {
  name = local.ssh_key_name
  public_key = file(var.ssh_public_key_filepath)
  labels = local.common_tags
}

resource "hcloud_server" "host01" {
  name = var.hostname
  image = var.image
  server_type = var.server_type
  datacenter = var.datacenter
  ssh_keys = [
    hcloud_ssh_key.terraform.id
  ]
  labels = local.common_tags
}

resource "hcloud_volume" "host01" {
  name = local.volume_name
  location = var.location
  format = "ext4"
  size = var.docker_data_volume_size
  labels = local.common_tags
}

resource "hcloud_volume_attachment" "host01" {
  volume_id = hcloud_volume.host01.id
  server_id = hcloud_server.host01.id
  automount = false
}

resource "null_resource" "host01_provisioning" {
  depends_on = [
    hcloud_server.host01
  ]
  triggers = {
    ipv4_address = hcloud_server.host01.ipv4_address
    ssh_private_key_filepath = var.ssh_private_key_filepath
  }
  connection {
    type = "ssh"
    host = self.triggers.ipv4_address
    user = "root"
    private_key = file(self.triggers.ssh_private_key_filepath)
  }
  provisioner "remote-exec" {
    inline = [
      "export DOCKER_VOLUME_LINUX_DEVICE=${hcloud_volume.host01.linux_device};",
      "/opt/scripts/provision.sh;"
    ]
  }
  provisioner "remote-exec" {
    when = destroy
    inline = [
      "/opt/scripts/deprovision.sh"
    ]
  }
}
