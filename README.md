# hcloud-docker-server

This Terraform module helps to create a single virtual server in Hetzner cloud ready to run Docker containers.

## Resources

- https://gitlab.com/apilandio/machine-images/docker-base
