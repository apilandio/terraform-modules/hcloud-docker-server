variable "hcloud_access_token" {
  type = string
  description = "The Hetzner cloud access token"
}

variable "ssh_public_key_filepath" {
  type = string
  description = "The ssh public key file path"
}

variable "ssh_private_key_filepath" {
  type = string
  description = "The ssh private key file path"
}

variable "image" {
  type = number
  description = "ID or name of the Image the Server is created from"
}

variable "server_type" {
  type = string
  description = "ID or name of the Server type this Server should be created with"
  default = "cx11"
}

variable "docker_data_volume_size" {
  type = number
  description = "The Docker volume size"
  default = 50
}

variable "hostname" {
  type = string
  description = "Name of the Server to create (must be unique per project and a valid hostname as per RFC 1123)"
}

variable "location" {
  type = string
  description = "Location to create the volume in"
  default = "nbg1"
}

variable "datacenter" {
  type = string
  description = "The datacenter name"
  default = "nbg1-dc3"
}
